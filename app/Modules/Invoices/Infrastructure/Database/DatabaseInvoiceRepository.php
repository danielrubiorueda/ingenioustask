<?php

namespace App\Modules\Invoices\Infrastructure\Database;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Aggregate\Invoice;
use App\Modules\Invoices\Domain\InvoiceRepositoryInterface;
use App\Modules\Invoices\Domain\ValueObject\BilledCompany;
use App\Modules\Invoices\Domain\ValueObject\Company;
use App\Modules\Invoices\Domain\ValueObject\InvoiceDate;
use App\Modules\Invoices\Domain\ValueObject\InvoiceDueDate;
use App\Modules\Invoices\Domain\ValueObject\InvoiceId;
use App\Modules\Invoices\Domain\ValueObject\InvoiceNumber;
use App\Modules\Invoices\Domain\ValueObject\InvoiceStatus;
use App\Modules\Invoices\Domain\ValueObject\Product;
use App\Modules\Invoices\Domain\ValueObject\Products;
use Illuminate\Support\Facades\DB;

final class DatabaseInvoiceRepository implements InvoiceRepositoryInterface 
{
    public function find(InvoiceId $id): Invoice
    {
        $invoice = DB::table('invoices')->find($id->value());
        $company = DB::table('companies')->find($invoice->company_id);
        $products = DB::table('invoice_product_lines')
            ->join('products', 'invoice_product_lines.product_id', '=', 'products.id')
            ->where('invoice_id', '=', $invoice->id)
            ->get();
        
        $domainProducts = array_map(function($product){
            return new Product(
                $product->name,
                $product->quantity,
                $product->price,
                $product->quantity * $product->price
            );
        }, $products->all());

        return new Invoice(
            new InvoiceId($invoice->id),
            new InvoiceStatus($invoice->status),
            new InvoiceNumber($invoice->number),
            new InvoiceDate($invoice->date),
            new InvoiceDueDate($invoice->due_date),
            new Company(
                $company->name,
                $company->street,
                $company->city,
                $company->zip,
                $company->phone
            ),
            new BilledCompany(
                $company->name,
                $company->street,
                $company->city,
                $company->zip,
                $company->phone,
                $company->email
            ),
            new Products($domainProducts),
        );
    }

    public function updateInvoiceStatus(InvoiceId $id, StatusEnum $status): void
    {
        DB::table('invoices')
            ->where('id', '=', $id->value())
            ->update(['status' => $status->value]);
    }
}
