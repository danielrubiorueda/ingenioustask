<?php

namespace App\Modules\Invoices\Infrastructure\Controller;

use App\Modules\Invoices\Application\Find\FindInvoiceQuery;
use App\Modules\Invoices\Application\Find\FindInvoiceQueryHandler;
use Illuminate\Http\JsonResponse;

final class ShowInvoiceController 
{
    public function __construct(
        private FindInvoiceQueryHandler $findInvoiceQueryHandler
    ) {}

    public function show(string $id): JsonResponse
    {
        $invoice = ($this->findInvoiceQueryHandler)(new FindInvoiceQuery($id));
        return response()->json($invoice->invoiceData());
    }
}
