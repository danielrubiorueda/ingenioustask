<?php

namespace App\Modules\Invoices\Infrastructure\Controller;

use App\Modules\Invoices\Application\Reject\RejectInvoiceCommand;
use App\Modules\Invoices\Application\Reject\RejectInvoiceCommandHandler;
use Illuminate\Http\JsonResponse;

final class RejectInvoiceController 
{
    public function __construct(
        private RejectInvoiceCommandHandler $rejectInvoiceCommandHandler
    ) {}

    public function reject(string $id): JsonResponse
    {
        try {
            ($this->rejectInvoiceCommandHandler)(new RejectInvoiceCommand($id));
            return response()->json('Invoice rejected');
        } catch (\Exception $ex) {
            return response()->json('Invoice already processed');
        }
    }
}
