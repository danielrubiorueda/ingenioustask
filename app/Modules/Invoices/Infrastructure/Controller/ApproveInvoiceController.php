<?php

namespace App\Modules\Invoices\Infrastructure\Controller;

use App\Modules\Invoices\Application\Approve\ApproveInvoiceCommand;
use App\Modules\Invoices\Application\Approve\ApproveInvoiceCommandHandler;
use Illuminate\Http\JsonResponse;

final class ApproveInvoiceController 
{
    public function __construct(
        private ApproveInvoiceCommandHandler $approveInvoiceCommandHandler
    ) {}

    public function approve(string $id): JsonResponse
    {
        try {
            ($this->approveInvoiceCommandHandler)(new ApproveInvoiceCommand($id));
            return response()->json('Invoice approved');
        } catch (\Exception $ex) {
            return response()->json('Invoice already processed');
        }
    }
}
