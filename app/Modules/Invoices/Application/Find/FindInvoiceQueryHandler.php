<?php

namespace App\Modules\Invoices\Application\Find;

use App\Modules\Invoices\Domain\Aggregate\Invoice;
use App\Modules\Invoices\Domain\InvoiceRepositoryInterface;
use App\Modules\Invoices\Domain\ValueObject\InvoiceId;

class FindInvoiceQueryHandler
{
    public function __construct(
        private InvoiceRepositoryInterface $invoiceRepositoryInterface
    ) {}

    public function __invoke(FindInvoiceQuery $query): Invoice
    {
        return $this->invoiceRepositoryInterface->find(new InvoiceId($query->id));
    }
}
