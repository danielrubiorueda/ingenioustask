<?php

namespace App\Modules\Invoices\Application\Find;

final readonly class FindInvoiceQuery
{
    public function __construct(
        public string $id
    ) {}
}
