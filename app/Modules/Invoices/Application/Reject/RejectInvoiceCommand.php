<?php

namespace App\Modules\Invoices\Application\Reject;

final readonly class RejectInvoiceCommand
{
    public function __construct(
        public string $id
    ) {}
}
