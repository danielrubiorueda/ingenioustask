<?php

namespace App\Modules\Invoices\Application\Approve;

use App\Domain\Enums\StatusEnum;
use App\Modules\Approval\Api\ApprovalFacadeInterface;
use App\Modules\Approval\Api\Dto\ApprovalDto;
use App\Modules\Invoices\Domain\Aggregate\Invoice;
use App\Modules\Invoices\Domain\InvoiceRepositoryInterface;
use App\Modules\Invoices\Domain\ValueObject\InvoiceId;
use Ramsey\Uuid\Uuid;

class ApproveInvoiceCommandHandler
{
    public function __construct(
        private InvoiceRepositoryInterface $invoiceRepositoryInterface,
        private ApprovalFacadeInterface $approvalFacadeInterface,
    ) {}

    public function __invoke(ApproveInvoiceCommand $command): void
    {
        $invoice = $this->invoiceRepositoryInterface->find(new InvoiceId($command->id));
        
        $this->approvalFacadeInterface->approve(new ApprovalDto(
            Uuid::fromString($command->id),
            StatusEnum::from($invoice->invoiceStatus()),
            Invoice::class
        ));

        $this->invoiceRepositoryInterface->updateInvoiceStatus(
            new InvoiceId($command->id),
            StatusEnum::APPROVED
        );
    }
}
