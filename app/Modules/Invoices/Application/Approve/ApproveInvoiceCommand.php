<?php

namespace App\Modules\Invoices\Application\Approve;

final readonly class ApproveInvoiceCommand
{
    public function __construct(
        public string $id
    ) {}
}
