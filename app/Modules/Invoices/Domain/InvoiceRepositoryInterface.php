<?php

namespace App\Modules\Invoices\Domain;

use App\Domain\Enums\StatusEnum;
use App\Modules\Invoices\Domain\Aggregate\Invoice;
use App\Modules\Invoices\Domain\ValueObject\InvoiceId;

interface InvoiceRepositoryInterface {
    public function find(InvoiceId $id): Invoice;
    public function updateInvoiceStatus(InvoiceId $id, StatusEnum $status): void;
}
