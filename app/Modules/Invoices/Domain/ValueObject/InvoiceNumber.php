<?php

namespace App\Modules\Invoices\Domain\ValueObject;

use App\Domain\ValueObject\Uuid;

final class InvoiceNumber extends Uuid {}
