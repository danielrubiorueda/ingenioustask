<?php

namespace App\Modules\Invoices\Domain\ValueObject;


final readonly class BilledCompany {
    public function __construct(
        public string $name,
        public string $streetAddress,
        public string $city,
        public string $zipcode,
        public string $phone,
        public string $email,
    ) {}
}
