<?php

namespace App\Modules\Invoices\Domain\ValueObject;

final readonly class InvoiceStatus {
    public function __construct(
        public string $status
    ) {}
}
