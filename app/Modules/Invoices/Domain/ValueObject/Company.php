<?php

namespace App\Modules\Invoices\Domain\ValueObject;


final readonly class Company {
    public function __construct(
        public string $name,
        public string $streetAddress,
        public string $city,
        public string $zipcode,
        public string $phone,
    ) {}
}
