<?php

namespace App\Modules\Invoices\Domain\ValueObject;

use InvalidArgumentException;

final class Products 
{
    private array $products;
    
    public function __construct(
        array $products
    ) {
        foreach ($products as $product) {
            $this->validate($product);
            $this->products[] = $product;
        }
    }

    public function all(): array
    {
        return $this->products;
    }

    private function validate(Product $product): void
    {
        if (!$product instanceof Product) {
            throw new InvalidArgumentException(sprintf("%s class only can store %s", self::class, Product::class));
        }
    }
}
