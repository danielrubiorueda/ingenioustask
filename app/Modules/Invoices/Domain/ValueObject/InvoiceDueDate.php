<?php

namespace App\Modules\Invoices\Domain\ValueObject;

use DateTimeImmutable;

final class InvoiceDueDate extends DateTimeImmutable {}
