<?php

namespace App\Modules\Invoices\Domain\ValueObject;

use DateTimeImmutable;

final class InvoiceDate extends DateTimeImmutable {}
