<?php

namespace App\Modules\Invoices\Domain\ValueObject;


final readonly class Product {
    public function __construct(
        public string $name,
        public int $quantity,
        public int $unitPrice,
        public int $total,
    ) {}
}
