<?php

namespace App\Modules\Invoices\Domain\ValueObject;

use App\Domain\ValueObject\Uuid;

final class InvoiceId extends Uuid {}
