<?php

namespace App\Modules\Invoices\Domain\Aggregate;

use App\Modules\Invoices\Domain\ValueObject\InvoiceId;
use App\Modules\Invoices\Domain\ValueObject\InvoiceStatus;
use App\Modules\Invoices\Domain\ValueObject\InvoiceNumber;
use App\Modules\Invoices\Domain\ValueObject\InvoiceDate;
use App\Modules\Invoices\Domain\ValueObject\InvoiceDueDate;
use App\Modules\Invoices\Domain\ValueObject\Company;
use App\Modules\Invoices\Domain\ValueObject\BilledCompany;
use App\Modules\Invoices\Domain\ValueObject\Products;

final class Invoice {
    public function __construct(
        private InvoiceId $invoiceId,
        private InvoiceStatus $invoiceStatus,
        private InvoiceNumber $invoiceNumber,
        private InvoiceDate $invoiceDate,
        private InvoiceDueDate $invoiceDueDate,
        private Company $company,
        private BilledCompany $billedCompany,
        private Products $products,
    ){}

    public function invoiceData(): array
    {
        return [
            'Invoice number' => $this->invoiceNumber->value(),
            'Invoice date' => $this->invoiceDate->format('Y-m-d H:i:s'),
            'Due date' => $this->invoiceDueDate->format('Y-m-d H:i:s'),
            'Company' => $this->company,
            'Billed company' => $this->billedCompany,
            'Products' => $this->products->all(),
            'Total price' => $this->totalPrice(),
        ];
    }

    public function invoiceStatus(): string
    {
        return $this->invoiceStatus->status;
    }

    private function totalPrice(): int
    {
        $total = 0;

        foreach ($this->products->all() as $product) {
            $total += $product->total;
        }
        
        return $total;
    }
}
