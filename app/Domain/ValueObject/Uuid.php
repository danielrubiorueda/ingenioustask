<?php

namespace App\Domain\ValueObject;

use InvalidArgumentException;
use Stringable;
use Ramsey\Uuid\Uuid as RamseyUuid;

abstract class Uuid implements Stringable
{
    public function __construct(protected string $value) {
        $this->isValid($value);
    }

    final public function value(): string
	{
		return $this->value;
	}

    public function __toString(): string
	{
		return $this->value();
	}

    private function isValid(string $value): void
    {
        if (!RamseyUuid::isValid($value)) {
			throw new InvalidArgumentException(sprintf('%s: Not allowed value <%s>.', self::class, $value));
		}
    }
}
