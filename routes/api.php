<?php

use App\Modules\Invoices\Infrastructure\Controller\ApproveInvoiceController;
use App\Modules\Invoices\Infrastructure\Controller\RejectInvoiceController;
use App\Modules\Invoices\Infrastructure\Controller\ShowInvoiceController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('invoice/approve/{id}', [ApproveInvoiceController::class, 'approve']);
Route::get('invoice/reject/{id}', [RejectInvoiceController::class, 'reject']);
Route::get('invoice/{id}', [ShowInvoiceController::class, 'show']);